---
title: Mandurah to Merredin
layout: post
---

We left on Sunday the 12th and headed to Merredin Tourist park.  I wanted to stop briefly at GCWA to look around.  I don’t know why but it didn’t really register that they’d be operating (it’s Sunday after all) and got there and talked with Stuart in the hanger.  He rang the flight line to ask about an “AF” (Air experience Flight?).  Before I knew it I was driven out to the bus and was waiting for a flight. Alex took me up - a young instructor with about 5 years with the club.  He asked if I wanted to do the take off but I eventually decided that he could do that and I’d take over afterward (how else could I get the photo below!).  He put us in a thermal and invited me to take over.  I did and was doing pretty well I think.  I certainly got lots of turning practice.  At one point I stalled out and had to recover.  It felt like he’d kicked me out.  I climbed to about 6000 feet!  Then went south (up wind) for a while trying for various thermals, then turned back (Alex seemed happy for me to fly indefinitely) and joined down wind at about 2000.  I flew the circuit and did the landing, though he handled the air breaks.  There was a gust and we lost speed, I pushed on the stick and “recovered well” he said.  It was a great experience for $200.  Bev said she enjoyed waiting in the van, chilling, and having a nap.

![On Tow](/blog/assets/IMG_4943-export.jpeg)
